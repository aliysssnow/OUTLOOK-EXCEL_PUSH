# OUTLOOK-EXCEL_PUSH

**PROJECT NAME: OUTLOOK-EXCEL_PUSH_created_by_ALIYSS**<br />
This Folder contains experimental features, which are advised to install only, if the author of the files recommends to install them.
Please do not install programs, files, documents without noticing the author beforehand.

**INSTALLATION**<br />
Copy VBAPROJECT_COPY_created_by_ALIYSS.bat to the C:\My Program Files Folder
Run VBAPROJECT_COPY_created_by_ALIYSS.bat

**If you are a USER:**<br />
Follow the Installation.

**If there is no other USER you are ADMIN:**<br />
Extract the 9-EXPERIMENTAL Folder to your Userprofil Folder with your Password.
Login to the VBAPROJECT_COPY_created_by_ALIYSS.bat as Admin with your Password.
Push the Programs to the %FTPSERVER%.
And afterwards download them as non Admin.

## Built With		
* [ReadMe](https://readme.io/)	
* [Outlook](https://products.office.com/en/outlook)	
* [Excel](https://products.office.com/en/excel)		
* [Visual Studio Express](https://www.visualstudio.com/vs/visual-studio-express/)
