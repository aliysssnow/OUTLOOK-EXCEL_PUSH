# DATA_REPLICATION_FILE_created_by_ALIYSS

**PROJECT NAME: DATA_REPLICATION_FILE_created_by_ALIYSS**<br />
This Folder contains experimental features, which are advised to install only, if the author of the files recommends to install them.
Please do not install programs, files, documents without noticing the author beforehand.

**INSTALLATION**<br />
Make a Shortcut and send it to the desired Location on your Computer.

**ADMIN**<br />
If you see the File VBAPROJECT_MASTER_created_by_ALIYSS.bat it means you are Admin.
All the programs now run over your Computer and changes will be overwritten to other users.
Be careful and all the best from ALIYSS.

**USAGE**<br />
Use this File to change previous SQL-Data to newer SQL-DATA. Try it out it is quite self-explanatory.
Afterward insert the Data into: http://www.sqlines.com/online and convert it.

**CHANGELOGS**<br />
v1.1.3.87
The Process is simple there shouldn't be any Problems.
Nevertheless testing before doing would be a natural precaution to take.
In case of Bugs or Errors contact the Admin.

v1.0.0.00
Currently there aren't any Changelogs, for this is a very new folder.
Stay tuned for more updates.
In case of Bugs or Errors contact the Admin.

**CREDITS**<br />
Eszter Petrovics 	- for alpha-testing and beta-testing
