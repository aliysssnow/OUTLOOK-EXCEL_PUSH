# VBA_OUTLOOK_created_by_ALIYSS

**PROJECT NAME: VBA_OUTLOOK_created_by_ALIYSS**<br />
This Folder contains experimental features, which are advised to install only, if the author of the files recommends to install them.
Please do not install programs, files, documents without noticing the author beforehand.

**INSTALLATION**<br />
Copy VBAPROJECT_COPY_created_by_ALIYSS.bat to "My Program Files" and run it.
Open THING_scripted_by_ALIYSS.xlsm in the 2-CONFIG_FILES Folder.

**ADMIN**<br />
If you see the File VBAPROJECT_MASTER_created_by_ALIYSS.bat it means you are Admin.
All the programs now run over your Computer and changes will be overwritten to other users.
Be careful and all the best from ALIYSS.

**USAGE**<br />
This is a manual installation version, which will be updated as soon as Aliyss understands the whole Process.
Aliyss will try to find a solution to automate everything as beautifully as possible. At the moment the most you can do is wait.

**CHANGELOGS**<br />
v2.6.3.82<br />
Updated the Configuration File: THING_created_by_ALIYSS
Further Improvements have been made.

v2.6.3.77<br />
Updated the Configuration File: THING_created_by_ALIYSS
It now works with the Configuration of BACKUPING_CONFIG.
Folder Categorization of the Backup Files has been improved.
Added: /1-BACKUP_FILES
In case of Bugs or Errors contact the Admin.

v2.6.2.75<br />
AUTOMATIC_TOFOLDER has been enabled and should now work flawlessly.
In case of Bugs or Errors contact the Admin.

v2.5.9.72<br />
Updated the Configuration File: THING_created_by_ALIYSS
It now works with the Configuration of AUTOMATIC_DOWNLOAD again.
AUTOMATIC_TOFOLDER has been disabled due to multiple user issues.
Don't worry, it will be back online soon.
I repeat AUTOMATIC_TOFOLDER does not work. Don't try it!
In case of Bugs or Errors contact the Admin.

v2.5.9.68<br />
Folder Categorization of the Configuration Files has been improved.
Added: /2-AUTOMATIC_DOWNLOAD
Added: /2-AUTOMATIC_TOFOLDER
In case of Bugs or Errors contact the Admin.

v2.4.9.47<br />
It should work. Like everything. (With a bit customization)
Stuff might not work.
Subject download works.
In case of Bugs or Errors contact the Admin.

v1.0.0.00<br />
Currently there aren't any Changelogs, for this is a very new folder.
Stay tuned for more updates.
In case of Bugs or Errors contact the Admin.

**CREDITS**<br />
Iain McIean 		- for alpha-testing
