VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Sheet1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True
Sub AUTOMATIC_DOWNLOAD_Chaos()
    
    User_Form_Automation_Download.Show
    
End Sub
Sub AUTOMATIC_DOWNLOAD_Mercur()
    
    Worksheets("Sheet1").Range("A21:A5124").Clear

    Dim objFSO As Object
    Dim objFolder As Object
    Dim objSubFolder As Object
    
    i = 21
    
    'Create an instance of the FileSystemObject
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    'Get the folder object
    Set objFolder = objFSO.GetFolder(Environ("USERPROFILE") & "\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" & "\1-CONFIG_FILES\2-AUTOMATIC_DOWNLOAD")
    For Each objSubFolder In objFolder.Subfolders
        Worksheets("Sheet1").Cells(i, 1).Value = objSubFolder.Name
        i = i + 1
    Next
    
End Sub
Sub AUTOMATIC_DOWNLOAD_Janus()
Call Shell("explorer.exe" & " " & Environ("USERPROFILE") & "\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" & "\1-CONFIG_FILES\2-AUTOMATIC_DOWNLOAD", vbNormalFocus)
End Sub
Sub AUTOMATIC_DOWNLOAD_Tartarus()
'Delete all files and subfolders
'Be sure that no file is open in the folder
    Dim FSO As Object
    Dim MyPath As String

    Set FSO = CreateObject("scripting.filesystemobject")

    MyPath = Environ("USERPROFILE") & "\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" & "\1-CONFIG_FILES\2-AUTOMATIC_DOWNLOAD"

    If Right(MyPath, 1) = "\" Then
        MyPath = Left(MyPath, Len(MyPath) - 1)
    End If

    If FSO.FolderExists(MyPath) = False Then
        MsgBox MyPath & " doesn't exist"
        Exit Sub
    End If

    On Error Resume Next
    'Delete files
    FSO.DeleteFile MyPath & "\*.*", True
    'Delete subfolders
    FSO.DeleteFolder MyPath & "\*.*", True
    On Error GoTo 0
    
    Worksheets("Sheet1").Range("A21:A5124").Clear

    Dim objFSO As Object
    Dim objFolder As Object
    Dim objSubFolder As Object
    
    i = 21
    
    'Create an instance of the FileSystemObject
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    'Get the folder object
    Set objFolder = objFSO.GetFolder(Environ("USERPROFILE") & "\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" & "\1-CONFIG_FILES\2-AUTOMATIC_DOWNLOAD")
    For Each objSubFolder In objFolder.Subfolders
        Worksheets("Sheet1").Cells(i, 1).Value = objSubFolder.Name
        i = i + 1
    Next
    
End Sub


Sub AUTOMATIC_TOFOLDER_Chaos()
    
    User_Form_Automation_ToFolder.Show
    
End Sub
Sub AUTOMATIC_TOFOLDER_Mercur()
    
    Worksheets("Sheet1").Range("A21:A5124").Clear

    Dim objFSO As Object
    Dim objFolder As Object
    Dim objSubFolder As Object
    
    i = 21
    
    'Create an instance of the FileSystemObject
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    'Get the folder object
    Set objFolder = objFSO.GetFolder(Environ("USERPROFILE") & "\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" & "\1-CONFIG_FILES\2-AUTOMATIC_TOFOLDER")
    For Each objSubFolder In objFolder.Subfolders
        Worksheets("Sheet1").Cells(i, 1).Value = objSubFolder.Name
        i = i + 1
    Next
    
End Sub
Sub AUTOMATIC_TOFOLDER_Janus()
Call Shell("explorer.exe" & " " & Environ("USERPROFILE") & "\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" & "\1-CONFIG_FILES\2-AUTOMATIC_TOFOLDER", vbNormalFocus)
End Sub
Sub AUTOMATIC_TOFOLDER_Tartarus()
'Delete all files and subfolders
'Be sure that no file is open in the folder
    Dim FSO As Object
    Dim MyPath As String

    Set FSO = CreateObject("scripting.filesystemobject")

    MyPath = Environ("USERPROFILE") & "\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" & "\1-CONFIG_FILES\2-AUTOMATIC_TOFOLDER"

    If Right(MyPath, 1) = "\" Then
        MyPath = Left(MyPath, Len(MyPath) - 1)
    End If

    If FSO.FolderExists(MyPath) = False Then
        MsgBox MyPath & " doesn't exist"
        Exit Sub
    End If

    On Error Resume Next
    'Delete files
    FSO.DeleteFile MyPath & "\*.*", True
    'Delete subfolders
    FSO.DeleteFolder MyPath & "\*.*", True
    On Error GoTo 0
    
    Worksheets("Sheet1").Range("A21:A5124").Clear

    Dim objFSO As Object
    Dim objFolder As Object
    Dim objSubFolder As Object
    
    i = 21
    
    'Create an instance of the FileSystemObject
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    'Get the folder object
    Set objFolder = objFSO.GetFolder(Environ("USERPROFILE") & "\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" & "\1-CONFIG_FILES\2-AUTOMATIC_TOFOLDER")
    For Each objSubFolder In objFolder.Subfolders
        Worksheets("Sheet1").Cells(i, 1).Value = objSubFolder.Name
        i = i + 1
    Next
    
End Sub

Sub BACKUPING_CONFIG_Mercur()
    
    Worksheets("Sheet1").Range("A21:A5124").Clear

    Dim objFSO As Object
    Dim objFolder As Object
    Dim objSubFolder As Object
    
    i = 21
    
    'Create an instance of the FileSystemObject
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    'Get the folder object
    Set objFolder = objFSO.GetFolder(Environ("USERPROFILE") & "\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" & "\1-BACKUP_FILES")
    For Each objSubFolder In objFolder.Subfolders
        Worksheets("Sheet1").Cells(i, 1).Value = objSubFolder.Name
        i = i + 1
    Next
    
End Sub
Sub BACKUPING_CONFIG_Janus()
Call Shell("explorer.exe" & " " & Environ("USERPROFILE") & "\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" & "\1-BACKUP_FILES", vbNormalFocus)
End Sub
Sub BACKUPING_CONFIG_Tartarus()
'Delete all files and subfolders
'Be sure that no file is open in the folder
    Dim FSO As Object
    Dim MyPath As String

    Set FSO = CreateObject("scripting.filesystemobject")

    MyPath = Environ("USERPROFILE") & "\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" & "\1-BACKUP_FILES"

    If Right(MyPath, 1) = "\" Then
        MyPath = Left(MyPath, Len(MyPath) - 1)
    End If

    If FSO.FolderExists(MyPath) = False Then
        MsgBox MyPath & " doesn't exist"
        Exit Sub
    End If

    On Error Resume Next
    'Delete files
    FSO.DeleteFile MyPath & "\*.*", True
    'Delete subfolders
    FSO.DeleteFolder MyPath & "\*.*", True
    On Error GoTo 0
    
    Worksheets("Sheet1").Range("A21:A5124").Clear

    Dim objFSO As Object
    Dim objFolder As Object
    Dim objSubFolder As Object
    
    i = 21
    
    'Create an instance of the FileSystemObject
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    'Get the folder object
    Set objFolder = objFSO.GetFolder(Environ("USERPROFILE") & "\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" & "\1-BACKUP_FILES")
    For Each objSubFolder In objFolder.Subfolders
        Worksheets("Sheet1").Cells(i, 1).Value = objSubFolder.Name
        i = i + 1
    Next
    
End Sub
Sub BACKUPING_CONFIG_Chaos()
    
   Dim Shex As Object
   Set Shex = CreateObject("Shell.Application")
   tgtfile = Environ("USERPROFILE") & "\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS\2-CONFIG_FILES\VBAPROJECT_BACKUP_created_by_ALIYSS.bat - Shortcut.lnk"
   Shex.Open (tgtfile)
    
End Sub
