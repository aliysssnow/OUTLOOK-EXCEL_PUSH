VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} User_Form_Automation_ToFolder 
   Caption         =   "Automate Categorization of Emails On Start Of OUTLOOK.EXE"
   ClientHeight    =   5355
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   10380
   OleObjectBlob   =   "User_Form_Automation_ToFolder.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "User_Form_Automation_ToFolder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CommandButton_TOFOLDER_Click()
    
    Dim FSO As Object
    Set FSO = CreateObject("Scripting.FileSystemObject")
    
    filLtin = User_Form_Automation_ToFolder.TextBox_filLtin.Value
    User_Form_Automation_ToFolder.TextBox_filLtin.Value = filLtin
    
        Dim filFldr
        ''Creates a new folder if the specified folder is not existent.
        If Len(Dir(Environ("USERPROFILE") & "\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" & "\1-CONFIG_FILES\2-AUTOMATIC_TOFOLDER\" & filLtin, vbDirectory)) = 0 Then
             MkDir Environ("USERPROFILE") & "\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" & "\1-CONFIG_FILES\2-AUTOMATIC_TOFOLDER\" & filLtin
        End If
         filFldr = Environ("USERPROFILE") & "\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" & "\1-CONFIG_FILES\2-AUTOMATIC_TOFOLDER\" & filLtin
        
        
    Dim emmSave As String
    emmSave = User_Form_Automation_ToFolder.TextBox_EmmSave.Value
    User_Form_Automation_ToFolder.TextBox_EmmSave.Value = emmSave
    
    Set Fileout = FSO.CreateTextFile(filFldr & "\DOWNLOAD_Configuration_emmSave.txt", True, True)
    Fileout.Write emmSave
    Fileout.Close


    Dim emlAddr As String
    emlAddr = User_Form_Automation_ToFolder.TextBox_emlAddr.Value
    emlAddr = Replace(emlAddr, ",", vbNewLine)
    emlAddr = Replace(emlAddr, " ", "")
    User_Form_Automation_ToFolder.TextBox_emlAddr.Value = emlAddr
    
    Set Fileout = FSO.CreateTextFile(filFldr & "\DOWNLOAD_Configuration_emlAddr.txt", True, True)
    Fileout.Write emlAddr
    Fileout.Close
    
    
    Dim emlSubj As String
    emlSubj = User_Form_Automation_ToFolder.TextBox_emlSubj.Value
    emlSubj = Replace(emlSubj, ",", vbNewLine)
    User_Form_Automation_ToFolder.TextBox_emlSubj.Value = emlSubj
    
    Set Fileout = FSO.CreateTextFile(filFldr & "\DOWNLOAD_Configuration_emlSubj.txt", True, True)
    Fileout.Write emlSubj
    Fileout.Close
    
    
    Dim emlBody As String
    emlBody = User_Form_Automation_ToFolder.TextBox_emlBody.Value
    emlBody = Replace(emlBody, ",", vbNewLine)
    User_Form_Automation_ToFolder.TextBox_emlBody.Value = emlBody
    
    Set Fileout = FSO.CreateTextFile(filFldr & "\DOWNLOAD_Configuration_emlBody.txt", True, True)
    Fileout.Write emlBody
    Fileout.Close
    
    
    emlAtch = User_Form_Automation_ToFolder.TextBox_emlAtch.Value
    emlAtch = Replace(emlAtch, "$", vbNewLine)
    emlAtch = Replace(emlAtch, " ", "")
    User_Form_Automation_ToFolder.TextBox_emlAtch.Value = emlAtch
    
    Set Fileout = FSO.CreateTextFile(filFldr & "\DOWNLOAD_Configuration_emlAtch.txt", True, True)
    Fileout.Write emlAtch
    Fileout.Close
End Sub
