@echo off & goto Terminal

:Terminal
echo _________________________________________________________________
echo !!! TERMINAL
echo.
echo.
echo [A] Admin Login        [I] Installation
echo [R] Restoration        [U] Uninstallation
echo [E] Exit
echo.
echo.
set INPUT=
set /P INPUT=Type input: %=%
cls
If /I "%INPUT%"=="A" goto AdminLogin
If /I "%INPUT%"=="R" goto Restore
If /I "%INPUT%"=="I" goto Installation
If /I "%INPUT%"=="U" goto Uninstallation
If /I "%INPUT%"=="E" goto Exit
echo Incorrect input & goto Terminal



:AdminLogin
echo _________________________________________________________________
echo !!! ADMIN LOGIN
echo !!! Please input your Password as the Admin.
echo.
echo.
set INPUT=
set /P INPUT=Password: %=%
cls
If /I "%INPUT%"=="MRM3rian_1" goto AdminTerminal
echo Incorrect input & goto Terminal

:AdminTerminal
echo _________________________________________________________________
echo !!! ADMIN TERMINAL
echo.
echo.
echo [S] Set Up             [P] Password Manager
echo [U] Upload             [E] Exit to Terminal
echo.
echo.
set INPUT=
set /P INPUT=Type input: %=%
cls
If /I "%INPUT%"=="S" goto AdminSetUp
If /I "%INPUT%"=="P" goto PasswordManager
If /I "%INPUT%"=="U" goto AdminUpload
If /I "%INPUT%"=="E" goto Terminal
echo Incorrect input & goto AdminTerminal


:PasswordManager
echo _________________________________________________________________
echo !!! ADMIN TERMINAL - PASSWORD MANAGER
echo.
echo.
echo [O] Outlook            [X] Excel
echo [E] Exit to Terminal
echo.
echo.
set INPUT=
set /P INPUT=Type input: %=%
cls
If /I "%INPUT%"=="O" goto PasswordManagerOutlook
If /I "%INPUT%"=="X" goto PasswordManagerExcel
If /I "%INPUT%"=="E" goto AdminTerminal
echo Incorrect input & goto AdminTerminal

:PasswordManagerOutlook
echo _________________________________________________________________
echo !!! ADMIN TERMINAL - PASSWORD MANAGER - OUTLOOK
echo.
echo.
echo [V] VBA_OUTLOOK_created_by_ALIYSS
echo [E] Exit to Terminal
echo.
echo.
set INPUT=
set /P INPUT=Type input: %=%
If /I "%INPUT%"=="V" goto PasswordManager_VBA_OUTLOOK_created_by_ALIYSS
cls
If /I "%INPUT%"=="E" goto PasswordManager
echo Incorrect input & goto PasswordManager

:PasswordManager_VBA_OUTLOOK_created_by_ALIYSS
echo _________________________________________________________________
echo ### RETREAVING: PASSWORD_VBA_OUTLOOK_created_by_ALIYSS
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
echo.
echo.
echo !!! Password copied to clipboard !!!
echo BORUB93XKCR75X6| clip
echo.
echo.
echo _________________________________________________________________
echo ### RETREAVING: Successfull
echo.
echo.
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
cls
goto PasswordManagerOutlook


:PasswordManagerExcel
echo _________________________________________________________________
echo !!! ADMIN TERMINAL - PASSWORD MANAGER - EXCEL
echo.
echo.
echo [C] COMPARE_EXEL_FILE_created_by_ALIYSS
echo [D] DATA_REPLICATION_created_by_ALIYSS
echo [E] Exit to Terminal
echo.
echo.
set INPUT=
set /P INPUT=Type input: %=%
If /I "%INPUT%"=="C" goto PasswordManager_COMPARE_EXEL_FILE_created_by_ALIYSS
If /I "%INPUT%"=="D" goto PasswordManager_DATA_REPLICATION_created_by_ALIYSS
cls
If /I "%INPUT%"=="E" goto PasswordManager
echo Incorrect input & goto PasswordManager

:PasswordManager_COMPARE_EXEL_FILE_created_by_ALIYSS
echo _________________________________________________________________
echo ### RETREAVING: PASSWORD_COMPARE_EXEL_FILE_created_by_ALIYSS
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
echo.
echo.
echo !!! Password copied to clipboard !!!
echo DGZC86YTXI5ISTR| clip
echo.
echo.
echo _________________________________________________________________
echo ### RETREAVING: Successfull
echo.
echo.
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
cls
goto PasswordManagerExcel

:PasswordManager_DATA_REPLICATION_created_by_ALIYSS
echo _________________________________________________________________
echo ### RETREAVING: PASSWORD_DATA_REPLICATION_created_by_ALIYSS
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
echo.
echo.
echo !!! Password copied to clipboard !!!
echo 5I4DDMYDOC1PTWN| clip
echo.
echo.
echo _________________________________________________________________
echo ### RETREAVING: Successfull
echo.
echo.
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
cls
goto PasswordManagerExcel

:AdminSetUp
echo _________________________________________________________________
echo !!! ADMIN TERMINAL - SET UP
echo Do you want to set Up your Admin Profil? (Y/N)
set INPUT=
set /P INPUT=Type input: %=%
cls
If /I "%INPUT%"=="y" goto AdminSetUpYes
If /I "%INPUT%"=="n" goto AdminTerminal
echo Incorrect input & goto AdminSetUp

:AdminSetUpYes
echo _________________________________________________________________
echo ### Set up: Admin Configuration
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul

if not exist "%USERPROFILE%\9-EXPERIMENTAL" mkdir "%USERPROFILE%\9-EXPERIMENTAL"


echo _________________________________________________________________
echo ### Set up: Successfull
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
cls
goto AdminTerminal

:AdminUpload
echo _________________________________________________________________
echo !!! ADMIN TERMINAL - UPLOAD
echo.
echo.
echo [O] Outlook            [X] Excel
echo [A] Upload All         [E] Exit to Terminal
echo.
echo.
set INPUT=
set /P INPUT=Type input: %=%
cls
If /I "%INPUT%"=="O" goto AdminUploadOutlook
If /I "%INPUT%"=="X" goto AdminUploadExcel
If /I "%INPUT%"=="A" goto AdminUploadAll
If /I "%INPUT%"=="E" goto AdminTerminal
echo Incorrect input & goto AdminTerminal

:AdminUploadOutlook
echo _________________________________________________________________
echo !!! ADMIN TERMINAL - UPLOAD - OUTLOOK
echo.
echo.
echo [V] VBA_OUTLOOK_created_by_ALIYSS
echo [E] Exit to Upload Terminal
echo.
echo.
set INPUT=
set /P INPUT=Type input: %=%
cls
If /I "%INPUT%"=="V" goto AdminUpload_VBA_OUTLOOK_created_by_ALIYSS
If /I "%INPUT%"=="E" goto AdminUpload
echo Incorrect input & goto AdminUpload

:AdminUpload_VBA_OUTLOOK_created_by_ALIYSS
echo _________________________________________________________________
echo ### UPLOAD: VBA_OUTLOOK_created_by_ALIYSS
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
XCOPY /s "%AppData%\Microsoft\Outlook" "%USERPROFILE%\9-EXPERIMENTAL\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS"
if not exist "%FTPSERVER%\9-EXPERIMENTAL" mkdir "%FTPSERVER%\9-EXPERIMENTAL"
if not exist "%FTPSERVER%\9-EXPERIMENTAL\OUTLOOK" mkdir "%FTPSERVER%\9-EXPERIMENTAL\OUTLOOK"
if not exist "%FTPSERVER%\9-EXPERIMENTAL\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" mkdir "%FTPSERVER%\9-EXPERIMENTAL\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS"
XCOPY /s "%USERPROFILE%\9-EXPERIMENTAL\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" "%FTPSERVER%\9-EXPERIMENTAL\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" /y
echo _________________________________________________________________
echo ### UPLOAD: Successfull
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
cls
goto AdminUploadOutlook

:AdminUploadExcel
echo _________________________________________________________________
echo !!! ADMIN TERMINAL - UPLOAD - EXCEL
echo.
echo.
echo [C] COMPARE_EXEL_FILE_created_by_ALIYSS
echo [D] DATA_REPLICATION_created_by_ALIYSS
echo [E] Exit to Upload Terminal
echo.
echo.
set INPUT=
set /P INPUT=Type input: %=%
cls
If /I "%INPUT%"=="C" goto AdminUpload_COMPARE_EXEL_FILE_created_by_ALIYSS
If /I "%INPUT%"=="D" goto AdminUpload_DATA_REPLICATION_created_by_ALIYSS
If /I "%INPUT%"=="E" goto AdminUpload
echo Incorrect input & goto AdminUpload


:AdminUpload_COMPARE_EXEL_FILE_created_by_ALIYSS
echo _________________________________________________________________
echo ### UPLOAD: COMPARE_EXEL_FILE_created_by_ALIYSS
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
if not exist "%FTPSERVER%\9-EXPERIMENTAL" mkdir "%FTPSERVER%\9-EXPERIMENTAL"
if not exist "%FTPSERVER%\9-EXPERIMENTAL\EXCEL" mkdir "%FTPSERVER%\9-EXPERIMENTAL\EXCEL"
if not exist "%FTPSERVER%\9-EXPERIMENTAL\EXCEL\COMPARE_EXEL_FILE_created_by_ALIYSS" mkdir "%FTPSERVER%\9-EXPERIMENTAL\EXCEL\COMPARE_EXEL_FILE_created_by_ALIYSS"
XCOPY /s "%USERPROFILE%\9-EXPERIMENTAL\EXCEL\COMPARE_EXEL_FILE_created_by_ALIYSS" "%FTPSERVER%\9-EXPERIMENTAL\EXCEL\COMPARE_EXEL_FILE_created_by_ALIYSS" /y
echo _________________________________________________________________
echo ### UPLOAD: Successfull
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
cls
goto AdminUploadExcel

:AdminUpload_DATA_REPLICATION_created_by_ALIYSS
echo _________________________________________________________________
echo ### UPLOAD: DATA_REPLICATION_created_by_ALIYSS
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
if not exist "%FTPSERVER%\9-EXPERIMENTAL" mkdir "%FTPSERVER%\9-EXPERIMENTAL"
if not exist "%FTPSERVER%\9-EXPERIMENTAL\EXCEL" mkdir "%FTPSERVER%\9-EXPERIMENTAL\EXCEL"
if not exist "%FTPSERVER%\9-EXPERIMENTAL\EXCEL\DATA_REPLICATION_created_by_ALIYSS" mkdir "%FTPSERVER%\9-EXPERIMENTAL\EXCEL\DATA_REPLICATION_created_by_ALIYSS"
XCOPY /s "%USERPROFILE%\9-EXPERIMENTAL\EXCEL\DATA_REPLICATION_created_by_ALIYSS" "%FTPSERVER%\9-EXPERIMENTAL\EXCEL\DATA_REPLICATION_created_by_ALIYSS" /y
echo _________________________________________________________________
echo ### UPLOAD: Successfull
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
cls
goto AdminUploadExcel

:AdminUploadAll
echo _________________________________________________________________
echo ### UPLOAD: ALL_FILES
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
if not exist "%FTPSERVER%\9-EXPERIMENTAL" mkdir "%FTPSERVER%\9-EXPERIMENTAL"
if not exist "%FTPSERVER%\9-EXPERIMENTAL\EXCEL" mkdir "%FTPSERVER%\9-EXPERIMENTAL\EXCEL"
if not exist "%FTPSERVER%\9-EXPERIMENTAL\EXCEL\SWO&XML_DISTRIBUTION_created_by_ALIYSS" mkdir "%FTPSERVER%\9-EXPERIMENTAL\EXCEL\SWO&XML_DISTRIBUTION_created_by_ALIYSS"
XCOPY /s "%USERPROFILE%\9-EXPERIMENTAL\EXCEL\SWO&XML_DISTRIBUTION_created_by_ALIYSS" "%FTPSERVER%\9-EXPERIMENTAL\EXCEL\SWO&XML_DISTRIBUTION_created_by_ALIYSS" /y
if not exist "%FTPSERVER%\9-EXPERIMENTAL\EXCEL\HOP_COMPRESSION_created_by_ALIYSS" mkdir "%FTPSERVER%\9-EXPERIMENTAL\EXCEL\HOP_COMPRESSION_created_by_ALIYSS"
XCOPY /s "%USERPROFILE%\9-EXPERIMENTAL\EXCEL\HOP_COMPRESSION_created_by_ALIYSS" "%FTPSERVER%\9-EXPERIMENTAL\EXCEL\HOP_COMPRESSION_created_by_ALIYSS" /y
if not exist "%FTPSERVER%\9-EXPERIMENTAL\EXCEL\COMPARE_EXEL_FILE_created_by_ALIYSS" mkdir "%FTPSERVER%\9-EXPERIMENTAL\EXCEL\COMPARE_EXEL_FILE_created_by_ALIYSS"
XCOPY /s "%USERPROFILE%\9-EXPERIMENTAL\EXCEL\COMPARE_EXEL_FILE_created_by_ALIYSS" "%FTPSERVER%\9-EXPERIMENTAL\EXCEL\COMPARE_EXEL_FILE_created_by_ALIYSS" /y
if not exist "%FTPSERVER%\9-EXPERIMENTAL\EXCEL\DATA_REPLICATION_created_by_ALIYSS" mkdir "%FTPSERVER%\9-EXPERIMENTAL\EXCEL\DATA_REPLICATION_created_by_ALIYSS"
XCOPY /s "%USERPROFILE%\9-EXPERIMENTAL\EXCEL\DATA_REPLICATION_created_by_ALIYSS" "%FTPSERVER%\9-EXPERIMENTAL\EXCEL\DATA_REPLICATION_created_by_ALIYSS" /y
if not exist "%FTPSERVER%\9-EXPERIMENTAL\OUTLOOK" mkdir "%FTPSERVER%\9-EXPERIMENTAL\OUTLOOK"
if not exist "%FTPSERVER%\9-EXPERIMENTAL\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" mkdir "%FTPSERVER%\9-EXPERIMENTAL\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS"
XCOPY /s "%USERPROFILE%\9-EXPERIMENTAL\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" "%FTPSERVER%\9-EXPERIMENTAL\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" /y

echo _________________________________________________________________
echo ### UPLOAD: Successfull
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
cls
goto AdminUpload


:Installation
echo _________________________________________________________________
echo !!! INSTALLATION
echo.
echo.
echo [O] Outlook            [X] Excel
echo [E] Exit to Terminal
echo.
echo.
set INPUT=
set /P INPUT=Type input: %=%
cls
If /I "%INPUT%"=="O" goto InstallationOutlook
If /I "%INPUT%"=="X" goto InstallationExcel
If /I "%INPUT%"=="E" goto Terminal
echo Incorrect input & goto Terminal

:InstallationOutlook
echo _________________________________________________________________
echo !!! INSTALLATION - OUTLOOK
echo.
echo.
echo [V] VBA_OUTLOOK_created_by_ALIYSS
echo [E] Exit to Installation Terminal
echo.
echo.
set INPUT=
set /P INPUT=Type input: %=%
cls
If /I "%INPUT%"=="V" goto Installation_VBA_OUTLOOK_created_by_ALIYSS
If /I "%INPUT%"=="E" goto Installation
echo Incorrect input & goto Installation

:Installation_VBA_OUTLOOK_created_by_ALIYSS
set hour=%time:~0,2%
if "%hour:~0,1%"==" " set hour=0%time:~1,1%
set folder=%date:~6,4%%date:~3,2%%date:~0,2%_%hour%_%time:~3,2%

echo _________________________________________________________________
echo ### INSTALLATION: VBA_OUTLOOK_created_by_ALIYSS
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul

if not exist "%USERPROFILE%\ALIYSS" mkdir "%USERPROFILE%\ALIYSS"
if not exist "%USERPROFILE%\ALIYSS\OUTLOOK" mkdir "%USERPROFILE%\ALIYSS\OUTLOOK"
if not exist "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" mkdir "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS"
if not exist "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS\1-BACKUP_FILES" mkdir "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS\1-BACKUP_FILES"
if not exist "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS\1-CONFIG_FILES" mkdir "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS\1-CONFIG_FILES"
if not exist "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS\1-CONFIG_FILES\2-AUTOMATIC_DOWNLOAD" mkdir "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS\1-CONFIG_FILES\2-AUTOMATIC_DOWNLOAD"
if not exist "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS\1-CONFIG_FILES\2-AUTOMATIC_TOFOLDER" mkdir "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS\1-CONFIG_FILES\2-AUTOMATIC_TOFOLDER"
if not exist "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS\2-CONFIG_FILES" mkdir "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS\2-CONFIG_FILES"
if not exist "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS\1-BACKUP_FILES\%folder%" mkdir "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS\1-BACKUP_FILES\%folder%"
xcopy /s "%appdata%\Microsoft\Outlook" "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS\1-BACKUP_FILES\%folder%"

xcopy /s "%FTPSERVER%\9-EXPERIMENTAL\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS\THING_scripted_by_ALIYSS.xlsm" "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS\2-CONFIG_FILES" /y
xcopy /s "%FTPSERVER%\9-EXPERIMENTAL\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS\VbaProject.OTM" "%appdata%\Microsoft\Outlook\" /y
xcopy /s "%FTPSERVER%\9-EXPERIMENTAL\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS\1-README (additional).txt" "%appdata%\Microsoft\Outlook\" /y

echo _________________________________________________________________
echo ### INSTALLATION: Successfull
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
cls
goto Installation



:InstallationExcel
echo _________________________________________________________________
echo !!! INSTALLATION - EXCEL
echo.
echo.
echo [C] COMPARE_EXEL_FILE_created_by_ALIYSS
echo [D] DATA_REPLICATION_created_by_ALIYSS
echo [H] HOP_COMPRESSION_created_by_ALIYSS
echo [S] SWO_XML_DISTRIBUTION_created_by_ALIYSS
echo [E] Exit to Installation Terminal
echo.
echo.
set INPUT=
set /P INPUT=Type input: %=%
cls
If /I "%INPUT%"=="C" goto Installation_COMPARE_EXEL_FILE_created_by_ALIYSS
If /I "%INPUT%"=="D" goto Installation_DATA_REPLICATION_created_by_ALIYSS
If /I "%INPUT%"=="H" goto Installation_HOP_COMPRESSION_created_by_ALIYSS
If /I "%INPUT%"=="S" goto Installation_SWO_XML_DISTRIBUTION_created_by_ALIYSS
If /I "%INPUT%"=="E" goto Installation
echo Incorrect input & goto Installation

:Installation_COMPARE_EXEL_FILE_created_by_ALIYSS
set hour=%time:~0,2%
if "%hour:~0,1%"==" " set hour=0%time:~1,1%
set folder=%date:~6,4%%date:~3,2%%date:~0,2%_%hour%_%time:~3,2%

echo _________________________________________________________________
echo ### INSTALLATION: COMPARE_EXEL_FILE_created_by_ALIYSS
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul

if not exist "%USERPROFILE%\ALIYSS" mkdir "%USERPROFILE%\ALIYSS"
if not exist "%USERPROFILE%\ALIYSS\EXCEL" mkdir "%USERPROFILE%\ALIYSS\EXCEL"
if not exist "%USERPROFILE%\ALIYSS\EXCEL\COMPARE_EXEL_FILE_created_by_ALIYSS" mkdir "%USERPROFILE%\ALIYSS\EXCEL\COMPARE_EXEL_FILE_created_by_ALIYSS"
xcopy /s "%FTPSERVER%\9-EXPERIMENTAL\EXCEL\COMPARE_EXEL_FILE_created_by_ALIYSS" "%USERPROFILE%\ALIYSS\EXCEL\COMPARE_EXEL_FILE_created_by_ALIYSS" /y

echo _________________________________________________________________
echo ### INSTALLATION: Successfull
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
cls
goto Installation

:Installation_DATA_REPLICATION_created_by_ALIYSS
set hour=%time:~0,2%
if "%hour:~0,1%"==" " set hour=0%time:~1,1%
set folder=%date:~6,4%%date:~3,2%%date:~0,2%_%hour%_%time:~3,2%

echo _________________________________________________________________
echo ### INSTALLATION: DATA_REPLICATION_created_by_ALIYSS
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul

if not exist "%USERPROFILE%\ALIYSS" mkdir "%USERPROFILE%\ALIYSS"
if not exist "%USERPROFILE%\ALIYSS\EXCEL" mkdir "%USERPROFILE%\ALIYSS\EXCEL"
if not exist "%USERPROFILE%\ALIYSS\EXCEL\DATA_REPLICATION_created_by_ALIYSS" mkdir "%USERPROFILE%\ALIYSS\EXCEL\DATA_REPLICATION_created_by_ALIYSS"
xcopy /s "%FTPSERVER%\9-EXPERIMENTAL\EXCEL\DATA_REPLICATION_created_by_ALIYSS" "%USERPROFILE%\ALIYSS\EXCEL\DATA_REPLICATION_created_by_ALIYSS" /y

echo _________________________________________________________________
echo ### INSTALLATION: Successfull
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
cls
goto Installation

:Restore
echo _________________________________________________________________
echo !!! RESTORATION
echo.
echo.
echo [O] Outlook
echo [E] Exit to Terminal
echo.
echo.
set INPUT=
set /P INPUT=Type input: %=%
cls
If /I "%INPUT%"=="O" goto RestoreOutlook
If /I "%INPUT%"=="E" goto Terminal
echo Incorrect input & goto Terminal

:RestoreOutlook
echo _________________________________________________________________
echo !!! RESTORATION - OUTLOOK
echo.
echo.
echo [V] VBA_OUTLOOK_created_by_ALIYSS
echo [E] Exit to Restoration Terminal
echo.
echo.
set INPUT=
set /P INPUT=Type input: %=%
cls
If /I "%INPUT%"=="V" goto Restore_VBA_OUTLOOK_created_by_ALIYSS_Confirmation
If /I "%INPUT%"=="E" goto Restore
echo Incorrect input & goto Restore

:Restore_VBA_OUTLOOK_created_by_ALIYSS_Confirmation
echo _________________________________________________________________
Echo !!! This will FORCE RESTORE from the 1-BACKUP_FILES
Echo !!! Do you wish to proceed? (Y/N)
set INPUT=
set /P INPUT=Type input: %=%
cls
If /I "%INPUT%"=="y" goto Restore_VBA_OUTLOOK_created_by_ALIYSS
If /I "%INPUT%"=="n" goto RestoreOutlook

:Restore_VBA_OUTLOOK_created_by_ALIYSS
set hour=%time:~0,2%
if "%hour:~0,1%"==" " set hour=0%time:~1,1%
set folder=%date:~6,4%%date:~3,2%%date:~0,2%_%hour%_%time:~3,2%
echo ____________________________________
echo ### BACKUP: VBA_OUTLOOK_created_by_ALIYSS
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
xcopy /s "%appdata%\Microsoft\Outlook" "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS\1-BACKUP_FILES\%folder%" /i
echo ____________________________________
echo ### BACKUP: Successfull
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
cls
echo ____________________________________
echo !!! From which Backup Folder would you like to restore?
echo !!! Example: %date:~6,4%%date:~3,2%%date:~0,2%_%hour%_%time:~3,2%
set INPUT=
set /P INPUT=Type input: %=%

echo ____________________________________
echo ### RESTORATION: VBA_OUTLOOK_created_by_ALIYSS
xcopy /s "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS\1-BACKUP_FILES\%INPUT%" "%appdata%\Microsoft\Outlook" /y
echo ____________________________________
echo ### RESTORATION: Successfull
cls
goto RestoreOutlook


:Uninstallation
echo _________________________________________________________________
echo !!! UNINSTALLATION
echo.
echo.
echo [O] Outlook            [X] Excel
echo [E] Exit to Terminal
echo.
echo.
set INPUT=
set /P INPUT=Type input: %=%
cls
If /I "%INPUT%"=="O" goto UninstallationOutlook
If /I "%INPUT%"=="X" goto UninstallationExcel
If /I "%INPUT%"=="E" goto Terminal
echo Incorrect input & goto Terminal

:UninstallationOutlook
echo _________________________________________________________________
echo !!! UNINSTALLATION - OUTLOOK
echo.
echo.
echo [V] VBA_OUTLOOK_created_by_ALIYSS
echo [E] Exit to Uninstallation Terminal
echo.
echo.
set INPUT=
set /P INPUT=Type input: %=%
cls
If /I "%INPUT%"=="V" goto Uninstallation_VBA_OUTLOOK_created_by_ALIYSS
If /I "%INPUT%"=="E" goto Uninstallation
echo Incorrect input & goto Uninstallation

:Uninstallation_VBA_OUTLOOK_created_by_ALIYSS
set hour=%time:~0,2%
if "%hour:~0,1%"==" " set hour=0%time:~1,1%
set folder=%date:~6,4%%date:~3,2%%date:~0,2%_%hour%_%time:~3,2%

echo _________________________________________________________________
echo ### UNINSTALLATION: VBA_OUTLOOK_created_by_ALIYSS
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul

IF EXIST "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" (
    rmdir "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" /s /q
)
IF EXIST "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" (
    rmdir "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" /s /q
)
IF EXIST "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" (
    rmdir "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" /s /q
)
IF EXIST "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" (
    rmdir "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" /s /q
)
IF EXIST "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" (
    rmdir "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" /s /q
)
IF EXIST "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" (
    rmdir "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" /s /q
)
IF EXIST "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" (
    rmdir "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" /s /q
)
IF EXIST "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" (
    rmdir "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" /s /q
)
IF EXIST "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" (
    rmdir "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" /s /q
)
IF EXIST "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" (
    rmdir "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" /s /q
)
IF EXIST "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" (
    rmdir "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" /s /q
)
IF EXIST "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" (
    rmdir "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" /s /q
)
IF EXIST "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" (
    rmdir "%USERPROFILE%\ALIYSS\OUTLOOK\VBA_OUTLOOK_created_by_ALIYSS" /s /q
)

echo _________________________________________________________________
echo ### UNINSTALLATION: Successfull
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
cls
goto UninstallationOutlook


:UninstallationExcel
echo _________________________________________________________________
echo !!! UNINSTALLATION - EXCEL
echo.
echo.
echo [C] COMPARE_EXEL_FILE_created_by_ALIYSS
echo [D] DATA_REPLICATION_created_by_ALIYSS
echo [E] Exit to Uninstallation Terminal
echo.
echo.
set INPUT=
set /P INPUT=Type input: %=%
cls
If /I "%INPUT%"=="C" goto Uninstallation_COMPARE_EXEL_FILE_created_by_ALIYSS
If /I "%INPUT%"=="D" goto Uninstallation_DATA_REPLICATION_created_by_ALIYSS
If /I "%INPUT%"=="E" goto Uninstallation
echo Incorrect input & goto Uninstallation

:Uninstallation_COMPARE_EXEL_FILE_created_by_ALIYSS
set hour=%time:~0,2%
if "%hour:~0,1%"==" " set hour=0%time:~1,1%
set folder=%date:~6,4%%date:~3,2%%date:~0,2%_%hour%_%time:~3,2%

echo _________________________________________________________________
echo ### UNINSTALLATION: COMPARE_EXEL_FILE_created_by_ALIYSS
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul

IF EXIST "%USERPROFILE%\ALIYSS\EXCEL\COMPARE_EXEL_FILE_created_by_ALIYSS" (
    rmdir "%USERPROFILE%\ALIYSS\EXCEL\COMPARE_EXEL_FILE_created_by_ALIYSS" /s /q
)
IF EXIST "%USERPROFILE%\ALIYSS\EXCEL\COMPARE_EXEL_FILE_created_by_ALIYSS" (
    rmdir "%USERPROFILE%\ALIYSS\EXCEL\COMPARE_EXEL_FILE_created_by_ALIYSS" /s /q
)
IF EXIST "%USERPROFILE%\ALIYSS\EXCEL\COMPARE_EXEL_FILE_created_by_ALIYSS" (
    rmdir "%USERPROFILE%\ALIYSS\EXCEL\COMPARE_EXEL_FILE_created_by_ALIYSS" /s /q
)
IF EXIST "%USERPROFILE%\ALIYSS\EXCEL\COMPARE_EXEL_FILE_created_by_ALIYSS" (
    rmdir "%USERPROFILE%\ALIYSS\EXCEL\COMPARE_EXEL_FILE_created_by_ALIYSS" /s /q
)
IF EXIST "%USERPROFILE%\ALIYSS\EXCEL\COMPARE_EXEL_FILE_created_by_ALIYSS" (
    rmdir "%USERPROFILE%\ALIYSS\EXCEL\COMPARE_EXEL_FILE_created_by_ALIYSS" /s /q
)

echo _________________________________________________________________
echo ### UNINSTALLATION: Successfull
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
cls
goto Uninstallation

:Uninstallation_DATA_REPLICATION_created_by_ALIYSS
set hour=%time:~0,2%
if "%hour:~0,1%"==" " set hour=0%time:~1,1%
set folder=%date:~6,4%%date:~3,2%%date:~0,2%_%hour%_%time:~3,2%

echo _________________________________________________________________
echo ### UNINSTALLATION: DATA_REPLICATION_created_by_ALIYSS
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul

IF EXIST "%USERPROFILE%\ALIYSS\EXCEL\DATA_REPLICATION_created_by_ALIYSS" (
    rmdir "%USERPROFILE%\ALIYSS\EXCEL\DATA_REPLICATION_created_by_ALIYSS" /s /q
)
IF EXIST "%USERPROFILE%\ALIYSS\EXCEL\DATA_REPLICATION_created_by_ALIYSS" (
    rmdir "%USERPROFILE%\ALIYSS\EXCEL\DATA_REPLICATION_created_by_ALIYSS" /s /q
)
IF EXIST "%USERPROFILE%\ALIYSS\EXCEL\DATA_REPLICATION_created_by_ALIYSS" (
    rmdir "%USERPROFILE%\ALIYSS\EXCEL\DATA_REPLICATION_created_by_ALIYSS" /s /q
)
IF EXIST "%USERPROFILE%\ALIYSS\EXCEL\DATA_REPLICATION_created_by_ALIYSS" (
    rmdir "%USERPROFILE%\ALIYSS\EXCEL\DATA_REPLICATION_created_by_ALIYSS" /s /q
)
IF EXIST "%USERPROFILE%\ALIYSS\EXCEL\DATA_REPLICATION_created_by_ALIYSS" (
    rmdir "%USERPROFILE%\ALIYSS\EXCEL\DATA_REPLICATION_created_by_ALIYSS" /s /q
)

echo _________________________________________________________________
echo ### UNINSTALLATION: Successfull
ping -n 2 127.0.0.1>nul
ping -n 2 127.0.0.1>nul
cls
goto Uninstallation

:Exit
echo _________________________________________________________________
echo !!! Do you want to quit?(Y/N)
set INPUT=
set /P INPUT=Type input: %=%
cls
If /I "%INPUT%"=="y" goto End2
If /I "%INPUT%"=="n" goto Terminal
echo Incorrect input & goto Terminal

:End2
echo _________________________________________________________________
    SET count=5
    SET genericMessage=!!! This window will close

    goto Output

:Output
    IF NOT %count% == -1 (
        cls
        IF %count% == 0 (
            echo %genericMessage% now.
        ) ELSE (
            echo %genericMessage% in %count% seconds.
        )
        SET /A count=%count% - 1
        ping localhost -n 2 >nul
        goto Output
    ) ELSE (
        exit
    )